# Copyright (C) 2013, Eric Wong <normalperson@yhbt.net> and all contributors
# License: AGPLv3 or later (https://www.gnu.org/licenses/agpl-3.0.txt)
#
# Mail Delivery Agent module, delivers mail into a ssoma git repo
package Ssoma::MDA;
use strict;
use warnings;
use Ssoma::GitIndexInfo;

sub new {
	my ($class, $git) = @_;
	bless { git => $git, ref => "refs/heads/master" }, $class;
}

# may convert existing blob to a tree
# returns false if message already exists
# returns true on successful delivery
sub blob_upgrade {
	my ($self, $gii, $new, $path) = @_;

	my $git = $self->{git};
	my $obj = "$self->{ref}^0:$path";
	my $cur = $git->blob_to_simple($obj);

	# do nothing if the messages match:
	return 0 if $git->simple_eq($cur, $new);

	# kill the old blob
	$gii->remove($path);

	# implicitly create a new tree via index with two messages
	foreach my $simple ($cur, $new) {
		my $id = $git->simple_to_blob($simple);
		my $path2 = $git->hash_simple2($simple);
		$gii->update("100644", $id, "$path/$path2");
	}
	1;
}

# used to update existing trees, which only happen when we have Message-ID
# conflicts
sub tree_update {
	my ($self, $gii, $new, $path) = @_;
	my $git = $self->{git};
	my $obj = "$self->{ref}^0:$path";
	my $cmd = "git ls-tree $obj";
	my @tree = `$cmd`;
	$? == 0 or die "$cmd failed: $!\n";
	chomp @tree;

	my $id = $git->simple_to_blob($new);
	my $path2 = $git->hash_simple2($new);

	# go through the existing tree and look for duplicates
	foreach my $line (@tree) {
		$line =~ m!\A100644 blob ([a-f0-9]{40})\t(([a-f0-9]{40}))\z! or
			die "corrupt repo: bad line from $cmd: $line\n";
		my ($xid, $xpath2) = ($1, $2);

		# do nothing if most of the message matches
		return 0 if $path2 eq $xpath2 || $id eq $xid;
	}

	# no duplicates found, add to the index
	$gii->update("100644", $id, "$path/$path2");
}

# this appends the given message-id to the git repo, requires locking
# (Ssoma::Git::sync_do)
sub append {
	my ($self, $path, $simple, $once) = @_;

	my $git = $self->{git};
	my $ref = $self->{ref};

	# $path is a path name we generated, so it's sanitized
	my $gii = Ssoma::GitIndexInfo->new;

	my $obj = "$ref^0:$path";
	my $cmd = "git cat-file -t $obj";
	my $type = `$cmd 2>/dev/null`;

	if ($? == 0) { # rare, object already exists
		chomp $type;
		if ($once) {
			my $mid = $simple->header("Message-ID");
			die "CONFLICT: Message-ID: $mid exists ($path)\n";
		}

		# we return undef here if the message already exists
		if ($type eq "blob") {
			# this may upgrade the existing blob to a tree
			$self->blob_upgrade($gii, $simple, $path) or return;
		} elsif ($type eq "tree") {
			# possibly add object to an existing tree
			$self->tree_update($gii, $simple, $path) or return;
		} else {
			# we're screwed if a commit/tag has the same SHA-1
			die "CONFLICT: `$cmd' returned: $type\n";
		}
	} else { # new message, just create a blob, common
		my $id = $git->simple_to_blob($simple);
		$gii->update('100644', $id, $path);
	}
	$git->commit_index($gii, 0, $ref, "mda");
}

# the main entry point takes an Email::Simple object
sub deliver {
	my ($self, $simple, $once) = @_;
	my $git = $self->{git};

	# convert the Message-ID into a path
	my $mid = $simple->header("Message-ID");

	# if there's no Message-ID, generate one to avoid too many conflicts
	# leading to trees
	if (!defined $mid || $mid =~ /\A\s*\z/) {
		$mid = '<' . $git->hash_simple2($simple) . '@localhost>';
		$simple->header_set("Message-ID", $mid);
	}
	my $path = $git->mid2path($mid);

	# kill potentially confusing/misleading headers
	foreach my $d (qw(lines content-length)) {
		$simple->header_set($d);
	}

	my $sub = sub {
		$git->tmp_index_do(sub {
			$self->append($path, $simple, $once);
		});
	};
	$git->sync_do(sub { $git->tmp_git_do($sub) });
}

1;
