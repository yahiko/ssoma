# Copyright (C) 2013, Eric Wong <normalperson@yhbt.net> and all contributors
# License: GPLv2 or later (https://www.gnu.org/licenses/gpl-2.0.txt)
#
# Note: some trivial code here stolen from git-svn + Perl modules
# distributed with git.  This remains GPLv2+ so improvements may flow
# back into git.  Note: git-svn has always been GPLv2+, unlike most
# of the rest of git being GPLv2-only.
#
# Not using Git.pm and friends directly because some git installations may use
# a different Perl than this (and I might end up rewriting this entirely
# in another language).  Git::IndexInfo is also somewhat recent, so folks
# on LTS distros may not have it, yet.

package Ssoma::GitIndexInfo;
use strict;
use warnings;

sub new {
	my ($class) = @_;
	my $pid = open my $gui, '|-';
	defined $pid or die "failed to pipe + fork: $!\n";
	if ($pid == 0) {
		exec(qw/git update-index -z --index-info/);
		die "exec failed: $!\n";
	}
	bless { gui => $gui, pid => $pid, nr => 0}, $class;
}

sub remove {
	my ($self, $path) = @_;
	print { $self->{gui} } '0 ', 0 x 40, "\t", $path, "\0" or
			die "failed to print to git update-index pipe: $!\n";
	++$self->{nr};
}

sub update {
	my ($self, $mode, $hash, $path) = @_;
	print { $self->{gui} } $mode, ' ', $hash, "\t", $path, "\0" or
			die "failed to print to git update-index pipe: $!\n";
	++$self->{nr};
}

sub done {
	my ($self) = @_;
	close $self->{gui} or die "close pipe: $!\n";
	$? == 0 or die "git update-index failed: $?\n";
}

1;
