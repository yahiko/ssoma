#!/usr/bin/perl -w
# Copyright (C) 2013, Eric Wong <normalperson@yhbt.net> and all contributors
# License: AGPLv3 or later (https://www.gnu.org/licenses/agpl-3.0.txt)
use strict;
use warnings;
use Test::More;
use Ssoma::MDA;
use Ssoma::Git;
use Email::Simple;
use Digest::SHA qw/sha1_hex/;
use File::Temp qw/tempdir/;

my $tmpdir = tempdir(CLEANUP => 1);
my $git = Ssoma::Git->new("$tmpdir/gittest");
$git->init_db;
my $mda = Ssoma::MDA->new($git);

my $email = Email::Simple->new("From: U <u\@example.com>\n\nHIHI\n");
my %headers = (
	"To" => "Me <me\@example.com>",
	"From" => "You <you\@example.com>",
	"Message-ID" => "<666\@example.com>",
	"Subject" => ":o",
	"Lines" => "666",
	"Content-Length" => "666",
);

my %discard = map { $_ => 1 } qw(Lines Content-Length);

while (my ($key, $val) = each %headers) {
	$email->header_set($key, $val);
}

$mda->deliver($email);

local $ENV{GIT_DIR} = "$tmpdir/gittest";

my $blob_id = sha1_hex("666\@example.com");
my ($dir, $base) = ($blob_id =~ m!\A([a-f0-9]{2})([a-f0-9]{38})\z!);
ok(defined $dir && defined $base, "bad sha1: $blob_id");

my $raw = `git cat-file blob HEAD:$dir/$base`;
is(0, $?, "git cat-file returned: $?");

my $delivered = Email::Simple->new($raw);
is("HIHI\n", $delivered->body, "body matches");

while (my ($key, $val) = each %headers) {
	if ($discard{$key}) {
		is($delivered->header($key), undef, "header $key discarded");
	} else {
		is($delivered->header($key), $val, "header $key not discarded");
	}
}

done_testing();

