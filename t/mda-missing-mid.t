#!/usr/bin/perl -w
# Copyright (C) 2013, Eric Wong <normalperson@yhbt.net> and all contributors
# License: AGPLv3 or later (https://www.gnu.org/licenses/agpl-3.0.txt)
use strict;
use warnings;
use Test::More;
use Ssoma::MDA;
use Ssoma::Git;
use Email::Simple;
use File::Temp qw/tempdir/;
my $tmpdir = tempdir(CLEANUP => 1);
my $git = Ssoma::Git->new("$tmpdir/gittest");
$git->init_db;
my $mda = Ssoma::MDA->new($git);
my $email = Email::Simple->new("From: U <u\@example.com>\n\nHIHI\n");
$mda->deliver($email);

local $ENV{GIT_DIR} = "$tmpdir/gittest";
my @tree = `git ls-tree -r HEAD`;
is(scalar @tree, 1, "one item in tree");
my @line = split(/\s+/, $tree[0]);
my $msg = Email::Simple->new($git->cat_blob($line[2]));
like($msg->header("message-id"), qr/\A<[a-f0-9]{40}\@localhost>\z/,
	"message-id generated for message missing it");

done_testing();
