#!/usr/bin/perl -w
# Copyright (C) 2013, Eric Wong <normalperson@yhbt.net> and all contributors
# License: AGPLv3 or later (https://www.gnu.org/licenses/agpl-3.0.txt)
use strict;
use warnings;
use Test::More;
use Ssoma::Extractor;
use Ssoma::Git;
use Ssoma::MDA;
use File::Temp qw/tempdir/;

my $mdadir = tempdir(CLEANUP => 1);
my $outdir = tempdir(CLEANUP => 1);

my $outgit = Ssoma::Git->new("$outdir/git");
my $ex = Ssoma::Extractor->new($outgit);
my $maildir = "$outdir/maildir/";
my $mailbox = "$outdir/mbox";

my $mdagit = Ssoma::Git->new("$mdadir/gittest");
$mdagit->init_db;
my $mda = Ssoma::MDA->new($mdagit);
my $email = Email::Simple->new(<<'EOF');
From: U <u@example.com>
To: Me <me@example.com>
Message-ID: <666@example.com>
Subject: :o

HIHI
EOF

$mda->deliver($email);

{
	my @cmd = (qw/git clone -q --mirror/,
			$mdagit->{git_dir}, $outgit->{git_dir});
	is(system(@cmd), 0, "extractor repository cloned");
}

{
	local $ENV{GIT_CONFIG} = "$outgit->{git_dir}/ssoma.state";
	is(system(qw/git config target.mydir.path/, $maildir), 0,
	   "setup maildir");
}


my $check_last = sub {
	my ($key) = @_;
	local $ENV{GIT_CONFIG} = "$outgit->{git_dir}/ssoma.state";
	my $last = `git config $key`;
	is($?, 0, "git config succeeds");
	like($last, qr/^[a-f0-9]{40}$/, "last-imported is a SHA1");
};

{
	$ex->extract("mydir");
	my @new = glob("$outdir/maildir/new/*");
	is(scalar @new, 1, "one file now exists in maildir");
	my $f = $new[0];
	open my $fh, '<', $f or die "opening $f failed: $!\n";
	local $/;
	my $s = <$fh>;
	my $simple = Email::Simple->new($s);
	is($simple->header('message-id'), '<666@example.com>',
		"delivered message-id matches");
	$check_last->("target.mydir.last-imported");
	unlink $f or die "failed to unlink $f: $!\n";
}

{
	local $ENV{GIT_CONFIG} = "$outgit->{git_dir}/ssoma.state";
	is(system(qw/git config target.mybox.path/, $mailbox), 0,
	   "setup mailbox");
}

{
	$ex->extract("mybox");
	open my $fh, '<', $mailbox or die "opening $mailbox failed: $!\n";
	local $/;
	my $s = <$fh>;
	my $simple = Email::Simple->new($s);
	is($simple->header('message-id'), '<666@example.com>',
			"delivered message-id matches");
	$check_last->("target.mybox.last-imported");
}

my $another = Email::Simple->new(<<'EOF');
From: U <u@example.com>
To: Me <me@example.com>
Message-ID: <666666@example.com>
Subject: byebye

*yawn*
EOF
$mda->deliver($another);

{
	local $ENV{GIT_DIR} = $outgit->{git_dir};
	is(system("git fetch -q"), 0, "fetching updates succeeds");
}

# ensure we can update maildir without adding old messages
{

	$ex->extract("mydir");
	my @new = glob("$outdir/maildir/new/*");
	is(scalar @new, 1, "one new file now exists in maildir");
	my $f = $new[0];
	open my $fh, '<', $f or die "opening $f failed: $!\n";
	local $/;
	my $s = <$fh>;
	my $simple = Email::Simple->new($s);
	is($simple->header('message-id'), '<666666@example.com>',
		"delivered message-id matches");
	is($simple->body, "*yawn*\n", "body matches");
	$check_last->("target.mydir.last-imported");
	unlink $f or die "failed to unlink $f: $!\n"; # for next test
}

# ensure we can update mmbox without adding old messages
{

	$ex->extract("mybox");
	open my $fh, '<', $mailbox or die "opening $mailbox failed: $!\n";
	my @lines = <$fh>;
	my @subjects = grep /^Subject:/, @lines;
	my @from_ = grep /^From /, @lines;
	is(scalar @subjects, 2, "2 subjects in mbox");
	is(scalar @from_, 2, "2 From_ lines in mbox");

	$check_last->("target.mydir.last-imported");
}

# ensure we can handle conflicts w/o reimporting when the MDA
# upgrades a blob to a tree.
my $conflict = Email::Simple->new(<<'EOF');
From: U <u@example.com>
To: Me <me@example.com>
Message-ID: <666666@example.com>
Subject: BYE

*YAWN*
EOF
$mda->deliver($conflict);

{
	local $ENV{GIT_DIR} = $outgit->{git_dir};
	is(system("git fetch -q"), 0, "fetching updates succeeds");
}

# ensure we can update maildir without adding old messages even on a
# message-id conflict
{

	$ex->extract("mydir");
	my @new = glob("$outdir/maildir/new/*");
	is(scalar @new, 1, "one new file now exists in maildir");
	my $f = $new[0];
	open my $fh, '<', $f or die "opening $f failed: $!\n";
	local $/;
	my $s = <$fh>;
	my $simple = Email::Simple->new($s);
	is($simple->header('message-id'), '<666666@example.com>',
		"delivered conflicting message-id matches");
	is($simple->body, "*YAWN*\n", "body matches on conflict");
	$check_last->("target.mydir.last-imported");
}

# ensure we can pipe to commands
{
	{
		my $cat = "cat >> $outdir/cat.out";
		local $ENV{GIT_CONFIG} = "$outgit->{git_dir}/ssoma.state";
		is(system(qw/git config target.cat.command/, $cat), 0,
		   "setup delivery command");
	}

	$ex->extract("cat");
	my $f = "$outdir/cat.out";
	open my $fh, '<', $f or die "open $f failed: $!\n";
	my @lines = <$fh>;
	my @subjects = grep /^Subject:/, @lines;
	my @from = grep /^From:/, @lines;
	my @mid = grep /^Message-ID:/i, @lines;
	is(scalar @subjects, 3, "3 subjects in dump");
	is(scalar @mid, 3, "3 message-ids in dump");
	is(scalar @from, 3, "3 From: lines in dump");

	$check_last->("target.cat.last-imported");
}

done_testing();
