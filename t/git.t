#!/usr/bin/perl -w
# Copyright (C) 2013, Eric Wong <normalperson@yhbt.net> and all contributors
# License: AGPLv3 or later (https://www.gnu.org/licenses/agpl-3.0.txt)
use strict;
use warnings;
use Test::More;
use Ssoma::Git;
use Ssoma::GitIndexInfo;
use File::Temp qw/tempdir/;
my $tmpdir = tempdir(CLEANUP => 1);
my $git = Ssoma::Git->new("$tmpdir/gittest");

$git->init_db;
ok(-d "$tmpdir/gittest", "git repo created");

{
	my $v = `GIT_DIR=$tmpdir/gittest git config ssoma.repoversion`;
	is(0, $?, "git config succeeded");
	chomp($v);
	is(1, $v, "ssoma.repoversion is set to 1");
}

is(0, $git->tmp_git_do(sub { system(qw(git config ssoma.test foo)) }),
   "setting config works");

is("foo\n", $git->tmp_git_do(sub { `git config ssoma.test` }),
   "reading config works");

$git->tmp_git_do(sub {
	my $commit;
	$git->tmp_index_do(sub {
		my $gii = Ssoma::GitIndexInfo->new;

		my $sha1 = `echo hello world | git hash-object -w --stdin`;
		is(0, $?, "hashed one object");
		chomp $sha1;

		is(1, $gii->update(100644, $sha1, 'hello/world'),
		   "add hashed object to index");
		$gii = undef;

		my $tree = `git write-tree`;
		is(0, $?, "wrote tree out");
		chomp $tree;

		$commit = `git commit-tree -m 'hi' $tree`;
		is(0, $?, "committed tree");
		chomp $commit;

		is(0, system(qw(git update-ref refs/heads/master), $commit),
		   "updated ref");
	});
});

{
	is($git->mid2path("<hello world>"),
	   $git->mid2path("\t<hello world>\t"),
	   "mid2path ignores leading/trailing whitespace");
}

done_testing();
